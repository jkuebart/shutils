" Import vim configuration.
source ~/.vim/vimrc

set belloff=
set guicursor=
set laststatus=1
set mouse=
set nohlsearch
set noincsearch
