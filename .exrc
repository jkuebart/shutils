set autoindent

" auto write on make
set autowrite

" No tabs <https://youtu.be/JbjpBs0QaUc?t=4m56s>
set expandtab

set ruler
set shiftwidth=4
" See https://github.com/tpope/vim-sensible/issues/16.
" set showmatch
set showmode

" Find tags files in higher-level directories
set tags=tags\ ../tags\ ../../tags\ ../../../tags\ ../../../../tags\ ../../../../../tags
