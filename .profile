PATH=${PATH:+$PATH:}$HOME/bin

BLOCKSIZE=1k
if command -v nvim >/dev/null
then
    EDITOR=nvim
else
    EDITOR=vi
fi
HOST=$(hostname)
export BLOCKSIZE EDITOR HOST

if [ OpenBSD = "$(uname -s)" ]
then
    # OpenBSD's less(1) has the -c option backwards.
    PAGER='less -ERXc'
else
    PAGER='less -ERX'
fi
export PAGER

# Some build systems expect $MAKE to be GNU make
BMAKE=~/bin/mak
export BMAKE

ENV=~/.shrc
export ENV
